CC=clang # or gcc

FRAMEWORKS:= -framework Foundation
#LIBRARIES:= -stdio

SOURCE=main.m

CFLAGS=-Wall -Werror -g $(SOURCE)
LDFLAGS=$(LIBRARIES) $(FRAMEWORKS)
OUT=InjectApplicationWrapper

all:
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(OUT)

run:
	make all; ./$(OUT) Safari

clean:
	rm $(OUT)