#include <stdio.h>
#include <stdlib.h>
#import <Foundation/Foundation.h>

NSString *escStr(NSString *string)
{
	return [string stringByReplacingOccurrencesOfString:@" " withString:@"\\ "];
}

int main(int argc, char **argv)
{
	NSString *launchPath = [[NSString stringWithFormat:@"%s", argv[0]] stringByDeletingLastPathComponent];
	NSBundle *appBundle = [NSBundle bundleWithPath:[[launchPath stringByDeletingLastPathComponent] stringByDeletingLastPathComponent]];
  	NSString *cmd = [NSString stringWithFormat:@"DYLD_INSERT_LIBRARIES=%@/Contents/MacOS/inject.dylib %@_", escStr(appBundle.bundlePath), escStr(appBundle.executablePath)];
  	
  	cmd = [cmd stringByAppendingString:@" &"];
  	printf("%s", cmd.UTF8String);
    system(cmd.UTF8String);
}